This is the repository for the **GELM** application.

The project **still in process** and you can find the work done and the future work
in the **documentation pdf**.

In the current version there are two main classes:
* Aida: This one has been developed for the evaluation of the application. 
* Main: This one is the application itself. It does the wikification of a given text.

The *Wikipedia* extractor used for this application is an adaption of **Wikistatsextractor** :
* https://github.com/diffbot/wikistatsextractor

You can find the modified extractor here:
* https://gitlab.com/jact/GELMextractor

In order to execute the application the H2 database *jar* should be added in the Class Path:
* http://www.h2database.com/html/download.html