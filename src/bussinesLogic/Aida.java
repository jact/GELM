package bussinesLogic;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import domain.Candidate;
/**
 * Class to process the Aida test in the GELM system.
*/
public class Aida {


	private static  String filein = "/gscratch2/users/jcampos004/2014wiki_aidatesta.in";
	private static  String fileout = "/gscratch2/users/jcampos004/2014wiki_aidatesta.out";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
				
		try {
		    BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8")); 
			BufferedWriter writer =new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(args[0]+ ".out")), "UTF-8"));
			String line = reader.readLine();
			long timea = System.currentTimeMillis();
			
			while(line!=null){
				
				String[] splitLine = line.split("\t");
				writer.write(splitLine[0] + "\t" + splitLine[1] + "\t" + splitLine[2] + "\t");

				if(splitLine.length<4){
				    System.out.println("Input error");
				    writer.write("NIL");
				    writer.write("\n");
				    line = reader.readLine();
				    continue;				    
				}
				
				Matcher matcher = new Matcher(null,null);				
				HashMap<String, Double> context = matcher.getContextCounts(splitLine[3]);				
				Disambiguator disambiguator = new Disambiguator(splitLine[2], context);
				ArrayList<Candidate> candidates = disambiguator.Disambiguate();
				if(candidates.size()==0)
				    writer.write("NIL");
				for(Candidate candidate:candidates){
					writer.write(candidate.gete().getName() + ":::" + candidate.getRank() + " ");
				}
				writer.write("\n");
				line = reader.readLine();
			}
		reader.close();
		writer.close();
		
		long timeb = System.currentTimeMillis();
		System.out.println("Denbora totala: " + (timeb-timea));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
