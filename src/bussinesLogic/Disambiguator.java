package bussinesLogic;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import dataAccess.DBManager;
import dataAccess.DBManager.DataNotFoundException;
import domain.Candidate;
import domain.Entity;
import domain.Mention;

/**
 *  The disambiguator class contains all the necessary information for the 
 *  disambiguation of a String s in a c context.
 */
public class Disambiguator {

	private Mention s; //Mention to be disambiguated
	private HashMap<String , Double> c; //An list containing the context of the mention.
	private ArrayList<Candidate> candidates = new ArrayList<Candidate>(); //List of candidates that have to be ranked.
	private DBManager db = DBManager.getInstance(); //Database manager instance
	
	/**
	 * @param String s, the name of the mention in text
	 * @param ArralyList<String> c the context of the mention
	 * Given the name of a mention and its context, instantiate a new disambiguator and  get all the 
	 * possible candidates.
	 */
	public Disambiguator (String s, HashMap<String , Double> c){		
		this.s = new Mention(s);		
		this.c = c;		
	}	
	
	/**
	 * This method will get the candidate Entities for the current mention
	 * and the f(s,*) of the mention.
	 */
	private void getCandidates(){
		try {
			
			String mentionInfo = db.getMentionInfo(this.s.gets());
			
			//If there are not candidates continue with next mention
			if(mentionInfo.contains("null"))
				return;
			
			//Split the info of the mention. It includes anchor_count and articles.
			String[] splitMention = mentionInfo.substring(1, mentionInfo.length()-1).split("\\),\\(");			
			
			//Set the anchor_count
			this.s.setfs_(Integer.parseInt(splitMention[0]));		
				
			//For each article-candidate get the entity information: f(*,e) and context
			for(int i = 1; i<splitMention.length ; i++){
				String[] splitArticle = splitMention[i].split(" ");
				try{				    
				    String entityInfo = db.getEntityInfo(splitArticle[0]);
					String[] splitEntity = entityInfo.split("%%%");			       

					HashMap<String, Float> bow = new HashMap<String, Float>();
					//If the context is null add empty hash map
					if(splitEntity[1].compareTo("(null)")!=0){
						for(int j = 1; j<splitEntity.length ; j++){						    
						    String[] contextWord = splitEntity[j].substring(1,splitEntity[j].length()-1).split(" ");
						    bow.put(contextWord[0], Float.parseFloat(contextWord[1]));
						}
					}
					//Create the candidate with the entity and add it to the candidates list.
					Entity entity = new Entity(splitArticle[0],Integer.parseInt(splitEntity[0].substring(1,splitEntity[0].length()-1)), bow);
					Candidate candidate = new Candidate(entity, Integer.parseInt(splitArticle[1]));
					candidates.add(candidate);
				} catch (DataNotFoundException e) {
					System.out.println(splitArticle[0] + " Entity not found");
				}					
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (DataNotFoundException e) {
			System.out.println(this.s.gets() + " Mention not found");
		}		
		
	}
	

	
	/**
	 * This method will get the rank for every candidate in 
	 * the disambiguation of the Mention s.
	 */
	public ArrayList<Candidate> Disambiguate(){		
		this.getCandidates();
		for(Candidate candidate:candidates){
			NaiveBayesProbabilitiesGenerator nB = new NaiveBayesProbabilitiesGenerator(candidate,this.s, this.c);
			candidate.setRank(nB.Calculate());
		}
		Collections.sort(candidates);	
		return candidates;
	}
	
	
	

}

