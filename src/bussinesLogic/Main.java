package bussinesLogic;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import dataAccess.DBManager;
import domain.Candidate;;

/**
 *  The main class of the GELM system where the document will be read
 *  and processed for the disambiguation of the entities that appear 
 *  in it.
 */
public class Main {

	private static String fileName = "proba.txt";
	private static String confFile = "conf/GELM.conf";
	private static DBManager db = DBManager.getInstance(); //Database manager instance
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//Load the properties fron the conf file.
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(confFile);
			prop.load(input);
		} catch (IOException ex) {
			ex.printStackTrace();
			return;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		try {
			String languageISO = prop.getProperty("language");
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			StringBuilder sb = new StringBuilder();
			String line;
			try {
				line = reader.readLine();
				while(line!=null){
					sb.append(line + " ");
					line = reader.readLine();
				}
				String document = sb.toString();
				
				Tokenizer tokenizer = new Tokenizer(document);		
				
				Matcher matcher = new Matcher(tokenizer.tokenize(), languageISO);
				
				long startTime = System.currentTimeMillis();
				ArrayList<String>  multitokens = matcher.match();
				long endTime = System.currentTimeMillis();
				long total = (endTime-startTime);
				System.out.println("The match has taken(s) " + total);
				
				startTime = System.currentTimeMillis();
				for(String multitoken : multitokens){
					Disambiguator disambiguator = new Disambiguator(multitoken ,matcher.getContext(multitokens, multitoken));
					ArrayList<Candidate> candidates = disambiguator.Disambiguate();
					if(candidates.isEmpty())
						continue;
					for(Candidate candidate : candidates)
						System.out.println(multitoken+ " ----> " + candidate.gete().getName() + " Rank : " +candidate.getRank());
				}				
				endTime = System.currentTimeMillis();
				total = (endTime-startTime);
				System.out.println("Disambiguation has taken(s) " +total);	
				
				try {
					db.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		

	}

}
