package bussinesLogic;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import dataAccess.DBManager;
import dataAccess.DBManager.DataNotFoundException;
/**
 *  The matcher class contains all the necessary methods for the 
 *  match function in which the tokens of the input document will be 
 *  matched with multitokens in original database (Wikipedia).
 */
public class Matcher {

	private ArrayList<String> document;
	private String languageISO;
	private int documentIndex=0;
	private DBManager db = DBManager.getInstance();
	
	public Matcher(ArrayList<String> document, String languageISO){
		this.document = document;
		this.languageISO = languageISO;
	}
	
	/**
	 * This function will match the longest multitoken matching each token of the document
	 * it will use the Match_Table from the database where the multitokens are
	 * ordered from longest to shortest.
	 */
	public ArrayList<String> match(){
		
		HashMap<String, Integer> stopWords = new HashMap<String, Integer>();
		
		try {
			for(String stopWord : db.getStopWords(languageISO).split(",")){
				stopWords.put(stopWord,1);
			}
		} catch (SQLException e1) {
			System.out.println("Problems to access the StopWords database");
			e1.printStackTrace();
		} catch (DataNotFoundException e1) {
			System.out.println("Nothing has been retrieved");
			e1.printStackTrace();
		}
		
		ArrayList<String> match = new ArrayList<String>();		
		
		//For each word in the document
		for(int i =0;i<document.size();i++){					
			String[] multitokens;
			int wordCount = 0;
			
			try {
				String textMultitoken = null;
				//Sorted from longest to shortest	
				multitokens = db.getMultiTokens(document.get(i)).split("%%%");
				
				//If no multitoken starting with this token
				if(multitokens == null)
					continue;
				
				for(String multitoken : multitokens){						
					
					//If the multitoken is an stopWord continue with the next one.
					if(stopWords.containsKey(multitoken))
						continue;
					
					int momentCount = multitoken.split(" ").length;
					
					
					//If out of document bounds continue with a shorter multitoken
					if(i+momentCount>document.size())
						continue;
					
					//Just generate the string from the document when the new multitoken length
					//is different from the one before
					if(wordCount != momentCount){
						wordCount = momentCount;
						StringBuilder sb = new StringBuilder();						
						for(int j = i;j<i+wordCount-1;j++){
							sb.append(document.get(j));
							sb.append(" ");
						}						
						sb.append(document.get(i+wordCount-1));
						textMultitoken = sb.toString();
					}
					
					//In this case add the multitoken to the match list
					//and increment the index of the document.
					if(textMultitoken.compareTo(multitoken)==0){
						match.add(multitoken);
						i += wordCount-1;
						break;
					}
				}
			} catch (SQLException  e) {
				System.out.println("Problems to access the Match database");
				e.printStackTrace();
			}catch (DataNotFoundException e){
			}	
		}
		//return the matched multitokens
		return match;		
	}
	
	
	/**
	 * This function will get the context of a token that will be disambiguated.
	 * It will consist on a window of 50.
	 * This context will only be made of tokens that has been matched.
	 * Apart from that, each context multitoken will be related with its anchor count.
	 */
	public HashMap<String , Double> getContext(ArrayList<String> multitokens, String multitoken){
		HashMap<String , Double> context = new HashMap<String , Double>();
		int start = 0;
		int end = 0;
		for(int index = this.documentIndex; index<document.size() ; index++){
			String token = document.get(index);
			//If there is a match get the window size and break
			if(multitoken.split(" ")[0].compareTo(token)!=0)
				continue;					
			if(index-50<0){
				start =0;
			}else{
				start = index-50;
			}
			if(index+50>document.size()){
				end = document.size()-1;								
			}else{
				end = index+50;
			}
			this.documentIndex = index;		
			break;
		}
		
		int multitokenIndex = 0;
		//For each multitoken in the context split it in tokens and get each probabilty.
		for(int i = start ; i<end; i++){
			if(multitokenIndex>=multitokens.size())
				break;
			String[] splitWord = multitokens.get(multitokenIndex).split(" ");
			int wordCount = splitWord.length;
			if(document.get(i).compareTo(splitWord[0]) == 0){
				if(multitokens.get(multitokenIndex).compareTo(multitoken)!=0){
					String key = multitokens.get(multitokenIndex);
					String[] tokens = key.split("_");
					for(String token : tokens){
						if(token.isEmpty())
							continue;
						try {
							context.put(token, (double) db.getfw_(token));
						} catch (SQLException | DataNotFoundException e1) {
							try{
								context.put(token,  (1d/db.getf__()));
								System.out.println(token +" not found in db");
							}catch(SQLException | DataNotFoundException e2) {
								System.out.println("Has not been added to context");
							}
						}	
					}
				}
				multitokenIndex++;
				i+=wordCount-1;
			}
		}		
		
		return context;
	}
	
	/**
	 * This function will be used only when we need to process a test input.
	 * This functions takes the context and gets the frequence of each token
	 * in it.
	 */
	public HashMap<String, Double> getContextCounts(String context){
		String[] splitContext = context.split(" +");
		HashMap<String , Double> contextCount = new HashMap<String, Double>();
		for(String multitoken : splitContext){
			if(multitoken.isEmpty())
				continue;
			try {
				contextCount.put(multitoken, (double) db.getfw_(multitoken));
			} catch (SQLException | DataNotFoundException e1) {
				try{
					contextCount.put(multitoken,  (1d/db.getf__()));
					System.out.println(multitoken +" not found in db");
				}catch(SQLException | DataNotFoundException e2) {
					System.out.println("Has not been added to context");
				}
			}			
		}		
		return contextCount;
	}
	
}
