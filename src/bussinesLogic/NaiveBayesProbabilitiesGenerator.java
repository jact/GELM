package bussinesLogic;

import java.sql.SQLException;
import java.util.HashMap;

import dataAccess.DBManager;
import dataAccess.DBManager.DataNotFoundException;
import domain.Candidate;
import domain.Entity;
import domain.Mention;
/**
 *  In this class every probability on our generative Bayesian Network will be calculated.
 */
public class NaiveBayesProbabilitiesGenerator {

	private Candidate candidate; //Candidate to be ranked
	private Mention s; //Mention to be disambiguated
	private HashMap<String , Double> c; //Context of the mention
	public double THETA = 0.9d; //Theta hyper-parameter
	public double LAMBDA = 0.9d; //Lambda hyper-parameter
	private DBManager db = DBManager.getInstance(); //Instance of the database manager
	
	/**
	 * @param Candidate candidate
	 * @param Mention s
	 * @param ArrayList<String> c
	 * Given a candidate, a mention and a bow instantiate a new probability calculator. 
	 */
	
	public NaiveBayesProbabilitiesGenerator(Candidate candidate, Mention s, HashMap<String , Double> c){
		this.candidate = candidate;
		this.s = s;
		this.c = c;	
	}		
	
	/**
	 * Function in which the three probabilities will be calculated and combined to return the rank of the candidate.
	 */
	public double Calculate(){		
		double Pe,Pse, Pce;
		try {
		    Pe = CalculatePe();
		    Pse = CalculatePse();
		    Pce = CalculatePce();
		    return Pe * Pse * Pce;	
		} catch (SQLException | DataNotFoundException e) {
		    System.out.println("Problem with the database when disambiguating "+ s.gets());
		}
		return -1d;
	}
	
	/**
	 * Function in which the Pe ,probability of generating entity e, will be calculated.
	 */
	private double CalculatePe() throws SQLException, DataNotFoundException{		
		return (candidate.gete().getf_e()+1d)/(db.getf__()+db.getN());
	}

	/**
	 * Function in which the Pse ,the mention probability, will be calculated.
	 */
	private double CalculatePse() throws SQLException, DataNotFoundException{
		double  a = THETA*candidate.getfse()/candidate.gete().getf_e();
		double b = (1d-THETA)*s.getfs_()/db.getf__();
		return a+b;
	}

	/**
	 * Function in which the Pce, the textual context, will be calculated.
	 */
	private double CalculatePce() throws SQLException, DataNotFoundException{
		double Pce = 1d;
		int n = c.size();
		Entity entity = candidate.gete();
		HashMap<String,Float> entityContext = entity.getC();
		for(String Key : c.keySet()){	
			int f__ = db.getf__();
			double fw_ = c.get(Key);
			double Pwe =((1d-LAMBDA)*(double)fw_/f__);
			if(entityContext.containsKey(Key)){
				Pwe += LAMBDA * entityContext.get(Key);
			}
			Pwe = Math.pow(Pwe, (double)1/n);
			Pce *= Pwe;
		}				
		return Pce;
	}
	

}
