package bussinesLogic;

import java.util.ArrayList;
/**
 * In this class a simple tokenizer will be implemented. 
 * This have to match with the one used in the extractor. 
 */
public class Tokenizer {
	
	private String document;
	
	
	public Tokenizer(String document){
		this.document = document;		
	}
	

	public ArrayList<String> tokenize(){
		String[] docArray = document.split("\\s+");
		ArrayList<String> tokenizedDocument = new ArrayList<String>();
		
		for(String multitoken:docArray){			
			if(!multitoken.matches(".*\\w+.*"))
				continue;	
			multitoken = multitoken.replaceAll("(\\w\\w\\w)\\.", "$1");
			multitoken =multitoken.replace(",","");
			multitoken =multitoken.replace("?","");
			multitoken =multitoken.replace("!","");
			multitoken = multitoken.replace("]" , "");
			multitoken = multitoken.replace("[" , "");
			multitoken = multitoken.replace(")" , "");
			multitoken = multitoken.replace("(" , "");								
			if(multitoken.length()==0)
				continue;
			tokenizedDocument.add(multitoken.toLowerCase());			
		}
		
		return tokenizedDocument;
		
	}
	
}
