package dataAccess;

import java.sql.Connection;

/**
 * Database manager implementation that will include the required sql
 * statements to retrieve information from H2 database.
 */

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBManager implements DBManagerInterface{
	/**
	 * Exception class that will be thrown when no data has been retrieved from the query.
	 */
	public class DataNotFoundException extends Exception{
		
		private static final long serialVersionUID = 1L;

		public DataNotFoundException(String message){
			super(message);
		}
	}	
	
	private int N = 0;
	private int fstst = 0;//f(*,*);
	private static DBManager theDBManager = null;
	private Connection con;
	private Statement stmt;
	
	/**
	 * @param args
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * Will create the connection with the embedded h2 database.
	 */
	private DBManager() throws SQLException, ClassNotFoundException{	
		long startTime = System.currentTimeMillis();
		Class.forName("org.h2.Driver");
		con = DriverManager.getConnection("jdbc:h2:/gscratch2/users/jcampos004/data/db/Hugedatabase", "", "" );
	    stmt = con.createStatement(); 
	    long endTime = System.currentTimeMillis();
	    System.out.println(endTime-startTime + " time to initialize the db");
	}
	
	/**
	 * getInstance method for single instance.
	 */
	public static DBManager getInstance() {
		if(theDBManager == null)
			try {
				return theDBManager = new DBManager();
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return theDBManager;
	}
	
	public void close() throws SQLException{
		con.close();
	}
	
	/**
	 * @see dataAccess.DBManagerInterface#getf__()
	 */
	@Override
	public int getf__() throws SQLException, DataNotFoundException {
		if(this.fstst==0){
			ResultSet rs = stmt.executeQuery("SELECT Data FROM Metadata WHERE Item = 'M'" );   
			
			if(!rs.next())
				throw new DataNotFoundException("Nothing has been retrieved from the query");
			
			this.fstst = Integer.parseInt(rs.getString("Data"));
		}
		return this.fstst;
	}
	
	/**
	 * @see dataAccess.DBManagerInterface#getN()
	 */
	@Override
	public int getN() throws SQLException, DataNotFoundException  {
		if(this.N==0){
			ResultSet rs = stmt.executeQuery("SELECT Data FROM Metadata WHERE Item = 'N'" );  
			
			if(!rs.next())
				throw new DataNotFoundException("Nothing has been retrieved from the query");
			
			this.N = Integer.parseInt(rs.getString("Data"));
		}
		return this.N;
	}	
	
	/**
	 * @see dataAccess.DBManagerInterface#getMentionInfo()
	 */
	@Override
	public String getMentionInfo(String s) throws SQLException , DataNotFoundException {
	    ResultSet rs = stmt.executeQuery("SELECT Anchor_Count, Articles FROM Multitoken_Dict WHERE Multitoken = '"+ s.replace("_", " ").replace("'", "''") + "'" );  
		
		if(!rs.next())
			throw new DataNotFoundException("Nothing has been retrieved from the query");	
		if(rs.getString("Articles")==null)
			return "(" + rs.getInt("Anchor_Count")+ ")," + rs.getString("Articles");
		return "(" + rs.getInt("Anchor_Count")+ ")," + rs.getString("Articles").replace("\'\'","\'");			
	}
	
	/**
	 * @see dataAccess.DBManagerInterface#getEntityInfo()
	 */
	@Override
	public String getEntityInfo(String e) throws SQLException , DataNotFoundException{
	    ResultSet rs = stmt.executeQuery("SELECT Count, Bow FROM Article_Counts WHERE Article = '"+ e.replace("'","''") + "'" ); 
		
		if(!rs.next()){
			throw new DataNotFoundException("Nothing has been retrieved from the query");
		}
		if(rs.getString("Bow")==null)
			return "(" + rs.getInt("Count") + ")%%%(" + rs.getString("Bow") + ")";
		return "(" + rs.getInt("Count") + ")%%%" + rs.getString("Bow").replace("\'\'","\'");
	}
	
	/**
	 * @see dataAccess.DBManagerInterface#getMultiTokens()
	 */
	@Override
	public String getMultiTokens(String token) throws SQLException , DataNotFoundException {
		ResultSet rs = stmt.executeQuery("SELECT * FROM Match WHERE Token = '"+ token+ "'" );  
		
		if(!rs.next())
			throw new DataNotFoundException("Nothing has been retrieved from the query");		
		
		return rs.getString("Multitokens").replace("\'\'","\'");		
		 
	}
	
	/**
	 * @see dataAccess.DBManagerInterface#getWordAppearances()
	 */
	@Override
	public int getfw_(String s) throws SQLException, DataNotFoundException{		
	    ResultSet rs = stmt.executeQuery("SELECT Anchor_Count FROM Multitoken_Dict WHERE Multitoken = '"+ s.replace("_" , " ").replace("'","''") + "'" ); 	
		
		if(!rs.next())
			throw new DataNotFoundException("Nothing has been retrieved from the query");	
		
		return rs.getInt("Anchor_Count");	
	}
	
	/**
	 * @see dataAccess.DBManagerInterface#getStopWords(java.lang.String)
	 */
	@Override
	public String getStopWords(String languageISO) throws SQLException,DataNotFoundException {
		
		ResultSet rs = stmt.executeQuery("SELECT Stopword_List FROM Stopwords WHERE Language = '"+ languageISO + "'" ); 	
		if(!rs.next())
			throw new DataNotFoundException("Nothing has been retrieved from the query");	
		
		return rs.getString("Stopword_List");	
	}

	

}
