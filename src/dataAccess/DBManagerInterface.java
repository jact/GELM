package dataAccess;

import java.sql.SQLException;
import dataAccess.DBManager.DataNotFoundException;

/**
 * Data Access Object Interface.
 */

public interface DBManagerInterface {
	
	/**
	 * Function that returns the total number of entity mention in Wikipedia.
	 * @return f(*,*)
	 * @throws SQLException
	 * @throws DataNotFoundException
	 */
	public int getf__() throws SQLException,  DataNotFoundException;
	
	/**
	 * Function that return the number of distinct entities in Wikipedia.
	 * @return N
	 * @throws SQLException
	 * @throws DataNotFoundException
	 */
	public int getN() throws SQLException,  DataNotFoundException;
	
	/**
	 * Given a token it returns all the multitokens starting with this token in descendant length order.
	 * @param token
	 * @return String Multitoken%%%Multitoken%%%Multitoken...
	 * @throws SQLException
	 * @throws DataNotFoundException
	 */
	public String getMultiTokens (String token) throws SQLException,  DataNotFoundException;

	/**
	 * Given a mention it returns the anchor_count and the articles related to the mention.
	 * @param s
	 * @return String (anchor_count),(article number of references),(article number of references)..
	 * @throws SQLException
	 * @throws DataNotFoundException
	 */
	public String getMentionInfo(String s) throws SQLException , DataNotFoundException ;
	
	/**
	 * Given an entity name it returns its context and number of mentions.
	 * @param e
	 * @return String number of mentions%%%word probability%%%word probability...
	 * @throws SQLException
	 * @throws DataNotFoundException
	 */
	public String getEntityInfo(String e) throws SQLException, DataNotFoundException;
	
	/**
	 * Given a word it return the number of times it has been used as an anchor in Wikipedia.
	 * @param s
	 * @return Appearances
	 * @throws SQLException
	 * @throws DataNotFoundException
	 */
	public int getfw_(String s) throws SQLException, DataNotFoundException;
	
	/**
	 * Given a language it returns the stopwords stored for this language
	 * @param languageISO
	 * @return StopWords
	 * @throws SQLException
	 * @throws DataNotFoundException
	 */
	public String getStopWords(String languageISO) throws SQLException, DataNotFoundException;
}
