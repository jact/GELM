package domain;

/**
 *  The Candidate class contains all the information related to a candidate:
 *  the entity if refers to, the times it has appeared as anchor for s and the rank
 *  given to it by our Bayesian network.
 */
public class Candidate implements Comparable<Candidate>{

	/**
	 * @param args
	 */
	private Entity e;
	private int fse;//f(s,e)
	private double rank = 0;//e = argmax P(e,s,c) = argmax P(e)P(s|e)P(c|e)
	
	public Candidate (Entity e, int fse){
		this.e = e;
		this.fse = fse;
	}
	
	public Entity gete() {
		return e;
	}
	public void sete(Entity e) {
		this.e = e;
	}
	public int getfse() {
		return fse;
	}
	public void setfse(int fse) {
		this.fse = fse;
	}
	public double getRank() {
		return rank;
	}
	public void setRank(double rank) {
		this.rank = rank;
	}
	@Override
	public int compareTo(Candidate o) {
		if(this.rank>o.rank)
			return -1;
		else if(this.rank<o.rank)
			return 1;
		return 0;
	}

}
