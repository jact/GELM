package domain;

import java.util.HashMap;

/**
 *  The entity class contains all the information related to an entity:
 *  its name and the number of times it appears in Wikipedia.
 */

public class Entity {
	
	private String name; //Entity name
	private int f_e; //f(*,e)
	private HashMap<String, Float> c; //Context c

	
	public Entity(String e, int f_e, HashMap<String, Float> c){
		this.name = e;
		this.f_e = f_e;
		this.c = c;
		}


	public String getName() {
		return name;
	}


	public void setName(String e) {
		this.name = e;
	}


	public int getf_e() {
		return f_e;
	}


	public void setf_e(int f_e) {
		this.f_e = f_e;
	}


	public HashMap<String, Float> getC() {
		return c;
	}


	public void setC(HashMap<String, Float> c2) {
		this.c = c2;
	}

}
