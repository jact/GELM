package domain;

/**
 *  The Mention class contains all the information related to a mention:
 *  its name and the number of times it appears in Wikipedia.
 */
public class Mention {
	
	private String s; //Mention name
	private int fs_;//f(s,*)
	
	public Mention(String s){
		this.s=s;	
	}

	public String gets() {
		return s;
	}

	public void sets(String s) {
		this.s = s;
	}

	public int getfs_() {
		return fs_;
	}

	public void setfs_(int fs_) {
		this.fs_ = fs_;
	}
}
